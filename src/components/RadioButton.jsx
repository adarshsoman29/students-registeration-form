/* eslint-disable react/prop-types */
import { useFormContext } from 'react-hook-form';
import { AnimatePresence, motion } from 'framer-motion';
import { MdError } from 'react-icons/md';
import { findInputError, isFormInvalid } from '../utils';

export default function RadioButton({ label, options, name, validation }) {
  const {
    register,
    formState: { errors },
  } = useFormContext();

  const inputError = findInputError(errors, name);
  const isInvalid = isFormInvalid(inputError);

  return (
    <>
      <div className="mt-5">
        <label className="block text-gray-700 font-semibold">{label}</label>
        <AnimatePresence mode="wait" initial={false}>
          {isInvalid && <InputError message={inputError.error.message} key={inputError.error.message} />}
        </AnimatePresence>
      </div>

      <div>
        {options.map((option, index) => (
          <label key={index} className="mr-2">
            <input type="radio" name={name} value={option.value} {...register(name, validation)} />
            {option.label}
          </label>
        ))}
      </div>
    </>
  );
}

const InputError = ({ message }) => {
  return (
    <motion.p className="flex items-center gap-1 px-2 font-semibold text-red-500 bg-red-100 rounded-md" {...framer_error}>
      <MdError />
      {message}
    </motion.p>
  );
};

const framer_error = {
  initial: { opacity: 0, y: 10 },
  animate: { opacity: 1, y: 0 },
  exit: { opacity: 0, y: 10 },
  transition: { duration: 0.2 },
};



