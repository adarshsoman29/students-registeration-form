import { Link } from "react-router-dom";
export default function WhyGurukulam() {
  return (
    <>
    
  <div className="bg-white p-6 rounded-lg shadow-md">
    <div className="flex items-center">
      <div className="flex-grow border-t border-gray-400"></div>
      <span className="mx-4 text-gray-600">
        <h1 className="text-2xl font-bold mb-4">Why gurukulam ?</h1>
      </span>
      <div className="flex-grow border-t border-gray-400"></div>
    </div>
  </div>


  <div className='flex m-10 gap-4'>
    <div className='rounded-md bg-zinc-100 p-5 flex flex-col justify-between'>
      <p className='font-serif font-bold italic'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Vel est quaerat aperiam molestias dolores dicta numquam eos, expedita totam repellendus sapiente praesentium sequi ex nostrum dolore ipsum, eligendi doloremque ut?</p>
      <br />
      <p className='text-amber-400 font-bold'>Alumni of 2017-2018 batch</p>
    </div>
    <div className='rounded-md bg-zinc-100 p-5'>
      <p className='font-serif font-bold italic flex flex-col justify-between'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum blanditiis cum impedit. Deleniti, excepturi quos maiores nam nisi eaque a dolores perferendis, earum iusto illum aperiam aliquam harum eum id.</p>
      <br />
      <p className='text-amber-400 font-bold'>Alumni of 2017-2018 batch</p>
    </div>
    <div className='rounded-md bg-zinc-100 p-5'>
      <p className='font-serif font-bold italic flex flex-col justify-between'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima sed consequatur assumenda quidem doloremque, quasi quisquam nisi voluptates voluptatibus corporis tenetur non molestias eum. Totam sint velit nostrum ipsum a?</p>
      <br />
      <p className='text-amber-400 font-bold'>Alumni of 2020-2021 batch</p>
    </div>
    <div className='rounded-md bg-zinc-100 p-5 flex flex-col justify-between'>
      <p className='font-serif font-bold italic'>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Blanditiis voluptatum magni repellat iusto amet magnam, sint quas sapiente libero facere odio similique numquam ipsum vel nulla laboriosam. Vero, at atque.</p>
      <br />
      <p className='text-amber-400 font-bold'>Alumni of 2021-2022 batch</p>
    </div>
    <div className='rounded-md bg-zinc-100 p-5 flex flex-col justify-between'>
      <p className='font-serif font-bold italic'>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Id et nostrum sequi praesentium dolorem illo necessitatibus libero. Alias illo deserunt ipsum aliquid voluptas architecto minus quas velit impedit, adipisci porro.</p>
      <br />
      <p className='text-amber-400 font-bold'>Alumni of 2022-2023 batch</p>
    </div>
  </div>
  <div className='flex  justify-center'> 
    <div className=''>
      <p className='text-2xl font-semibold'>Are you excited? come <Link className='max-w-md hover:bg-rose-600 mx-auto bg-rose-950 p-4 text-white font-semibold px-4 rounded-md'>join with us</Link></p>
    </div>
  </div>

  
    </>
  )
}
