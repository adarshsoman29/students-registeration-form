import { Link } from "react-router-dom";

export default function MainPhoto() {
  return (
    <>
    <div className='relative '>
    <img src="src/img/kids.jpg" alt="kids" className="w-full h-96  object-cover"/>
    <div className="absolute inset-0 bg-black opacity-50"></div>
    <h1 className='absolute top-20 left-24 text-3xl font-semibold text-white font-serif'>WELCOME TO GURUKULAM</h1>
    <p className='absolute  top-28 left-24 text-white text-lg  font- lora italic semibold text-base'>Dear students,
    <br />
    We are egerly waiting to welcome you to adorn our garden of education
    </p>
    <Link to='/registeration' 
    className="max-w-md mx-auto p-4 absolute top-40 left-24 bg-rose-950 hover:bg-rose-600 mt-5 text-white font-semibold py-2 px-4 rounded-md "
    >register here</Link>
    </div>

    </>
  )
}
