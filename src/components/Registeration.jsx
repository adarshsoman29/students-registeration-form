/* eslint-disable react/no-unescaped-entities */
import { FormProvider, useForm } from "react-hook-form"
import {yupResolver} from '@hookform/resolvers/yup'
import {BsFillCheckSquareFill} from 'react-icons/bs'
import { useState } from "react"
import Input from "../Input"
import { Medium_Validation, Name_Of_father_validation,
     Name_Of_mother_validation,
      PhoneNumber_Validation,
      Syllabus_Validation,
      address_Validation,
      class_validation, 
      date_of_birth_validation, 
      email_Validation, 
      name_Of_school_validation,
       name_of_student_validation, 
       whatsappNumber_Validation} from "../utils/inputValidations"
import { validationSchema } from "../utils/validationSchema"
import Select from "./Selelct"
import RadioButton from "./RadioButton"
import Textarea from "./textarea"

export default function Registeration() {
    const methods = useForm(
        { resolver : yupResolver(validationSchema) }
    )

    const [success, setSuccess] = useState(false)

    const onSubmit = methods.handleSubmit(data=>{
        console.log(data)
        methods.reset()
        setSuccess(true)
    })
  return (
    <>
    <div className="max-w-md mx-auto p4">
    <h1 className="text-2xl font-bold mb-4 mt-5">STUDENT'S REGISTRATION FORM</h1>  
    
    <FormProvider {...methods}>
        <form 
        onSubmit={onSubmit}
        noValidate
        autoComplete="off"
        >
            <div>
                <Input {...name_of_student_validation}></Input>
                <Input {...name_Of_school_validation}></Input>
                <Select {...class_validation}></Select>
                <RadioButton {...Syllabus_Validation}></RadioButton>
                <RadioButton {...Medium_Validation}></RadioButton>
                <Input {...date_of_birth_validation}></Input>
                <Input {...Name_Of_father_validation}></Input>
                <Input {...Name_Of_mother_validation}></Input>
                <Textarea{...address_Validation}></Textarea>
                <Input {...PhoneNumber_Validation}></Input>
                <Input {...whatsappNumber_Validation}></Input>
                <Input {...email_Validation}></Input>
            </div>

            <div className="mt-5"> 
            {
          success && (
            <p
            className="flex items-center gap-1 mb-5 font-semibold text-green-500">
              <BsFillCheckSquareFill/>Form has been submitted successfully
            </p>
          )
        }  
            </div>

            <button
        onSubmit={onSubmit}
        className="bg-rose-950 hover:bg-rose-600 mt-5 text-white font-semibold py-2 px-4 rounded-md w-full"
        >Submit Form</button>

        </form>
    </FormProvider>
    </div>
    
    </>
  )
}
