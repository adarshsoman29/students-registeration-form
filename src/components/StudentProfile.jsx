/* eslint-disable react/no-unescaped-entities */

export default function StudentProfile() {
  return (
    <>
    <h1 className="text-center text-3xl font-mono font-bold">Profile</h1>
    <h2>Name</h2>
    <div className="flex justify-center">
    <div className="flex gap-5">
      <img src="https://p7.hiclipart.com/preview/402/235/698/businessperson-computer-icons-avatar-passport.jpg" alt="" 
      className="w-32 h-32"
      />
      <div>
        <p><span>Class : </span> VII</p>
        <p><span>School : </span>B.H.S.S karungappally</p>
        <p><span>Medium : </span>english</p>
        <p><span>Syllabus : </span>state</p>
        <p><span>Address : </span>Lorem ipsum dolor sit amet consectetur,</p>
        <p><span>Date of birth : </span>13</p>
        <p><span>Father's name</span>Soman</p>
        <p><span>Mother's name : </span>Radhamany</p>
        <p><span>Phone number : </span>944611XXXX</p>
        <p><span>Whatsapp number</span> 944611XXXX</p>
      </div>
    </div>
    </div>
    </>
  )
}
