//  import { yupResolver } from "@hookform/resolvers/yup"
//  import { FormProvider, useForm } from "react-hook-form"
//  import {BsFillCheckSquareFill} from 'react-icons/bs'
//  import { validationSchema } from "../utils/validationSchema"
//  import { useState } from "react"
//  import Input from "../Input"
//  import { PhoneNumber_Validation,
//       email_Validation,
//        message_Validation, 
//        name_validation,
//         subject_validation } from "../utils/inputValidations"
//  import Textarea from "./textarea"


// export default function ContactUs() {
//      const methods = useForm(
//         {resolver : yupResolver(validationSchema)}
//      ) 
//      const [success, setSuccess] = useState(false)
//      const onSubmit = methods.handleSubmit(data=>{
//          console.log(data);
//          methods.reset();
//          setSuccess(true)
//      })
//   return (
//     <>
//     <div className="bg-white p-6 rounded-lg shadow-md">
//     <div className="flex items-center">
//       <div className="flex-grow border-t border-gray-400"></div>
//       <span className="mx-4 text-gray-600">
//         <h1 className="text-2xl font-bold mb-4">Get in touch</h1>
//       </span>
//       <div className="flex-grow border-t border-gray-400"></div>
//     </div>
//   </div>
 

//   <div className="flex justify-around items-center bg-slate-100 mt-2">
//     <ul>
//         <li><h4 className="font-semibold">Gurukulam</h4></li>
//         <li>Kallelibhagom</li>
//         <li>PO Karungappally, Kollam</li>
//         <li>Kerala, India</li>
//         <li>Pin : 690519</li>
//     </ul>

//     <ul>
//         <li className="flex"><h4 className="font-semibold">Phone : </h4> 94461137XX</li>
//     </ul>
//     <ul>
//         <li className="flex"><h4 className="font-semibold">Email : </h4> gurukulam@gmail.com</li>
//     </ul>

//     <div>
//         <h4 className="font-semibold">Location info :</h4>
//     {/* <iframe
//         title="Google Maps"
//         src="https://www.google.com/maps/dir/9.0654423,76.5314817/3H34%2B9FQ+GURUKULAM+Tuition+Centre,+Kallelibhagom,+Kerala+690523/@9.0583257,76.5233783,14z/data=!3m1!4b1!4m9!4m8!1m1!4e1!1m5!1m1!1s0x3b060473a2e8314f:0xda876306747569ac!2m2!1d76.5562414!2d9.0534758?entry=ttu"
//         width="600"
//         height="450"
//         style={{ border: 0 }}
//         allowFullScreen=""
//         loading="lazy"
//       ></iframe> */}
//     </div>
//   </div>
//   <div className="bg-white p-6 rounded-lg shadow-md">
//   <div className="bg-white p-6 rounded-lg shadow-md">
//     <div className="flex items-center">
//       <div className="flex-grow border-t border-gray-400"></div>
//       <span className="mx-4 text-gray-600">
//         <h1 className="text-2xl font-bold mb-4">Have a Question?:</h1>
//       </span>
//       <div className="flex-grow border-t border-gray-400"></div>
//     </div>
//   </div>
//   <p>Feel free to get in touch for any doubts or queries you may have!</p>
//   <div className="max-w-md mx-auto p4  bg-slate-100 p-6 rounded-lg shadow-md">
//     <FormProvider {...methods}>
//         <form 
//         onSubmit={onSubmit}
//         noValidate
//         autoCorrect="off"
//         >
//             <Input {...name_validation}></Input>
//             <Input {...PhoneNumber_Validation}></Input>
//             <Input {...email_Validation}></Input> 
//             <Input {...subject_validation}></Input>
//             <Textarea{...message_Validation}></Textarea>
//             <div className="mt-5">
//             {
//           success && (
//             <p
//             className="flex items-center gap-1 mb-5 font-semibold text-green-500">
//               <BsFillCheckSquareFill/>Form has been submitted successfully
//             </p>
//           )
//         } 
//         </div>
//         <button
//         onSubmit={onSubmit}
//         className="bg-rose-950 hover:bg-rose-600 mt-5 text-white font-semibold py-2 px-4 rounded-md w-full"
//         >Submit Form</button>
//         </form>
//     </FormProvider>
//   </div>
//   </div>
//     </>
//   )
// }
