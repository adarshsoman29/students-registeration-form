
export default function Disciplines() {
  return (
    <>
      <div className="bg-white p-6 rounded-lg shadow-md">
    <div className="flex items-center">
      <div className="flex-grow border-t border-gray-400"></div>
      <span className="mx-4 text-gray-600">
        <h1 className="text-2xl font-bold mb-4">Our disciplines</h1>
      </span>
      <div className="flex-grow border-t border-gray-400"></div>
    </div>
  </div>

  <div className='grid grid-cols-3 mt-5 pt-5 pl-20 mx-10'>
    <img src="src/img/cbsecrop.jpg" alt="" className='rounded-lg shadow-md w-80 h-auto' />
    <img src="src/img/state.jpg" alt=""  className='rounded-lg shadow-md w-80 h-auto'/>
    <img src="src/img/plus two.jpg" alt=""  className='rounded-lg shadow-md w-80 h-auto'/>
  </div>
    </>
  )
}
