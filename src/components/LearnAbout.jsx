

export default function LearnAbout() {
  return (
    <>
    <div className="bg-white p-6 rounded-lg shadow-md">
    <div className="flex items-center">
      <div className="flex-grow border-t border-gray-400"></div>
      <span className="mx-4 text-gray-600">
        <h1 className="text-2xl font-bold mb-4">Learn more about us</h1>
      </span>
      <div className="flex-grow border-t border-gray-400"></div>
    </div>
  </div>
  <div className='w-1/2 mt-5 ml-24 pt-10 pl-5 bg-slate-100 rounded-md shadow-md'>
    <p className='text-xl font-sans'>
      Lorem ipsum dolor, sit amet consectetur adipisicing elit. Soluta cum, voluptatibus tempora illo consequatur quam nostrum molestiae. Dignissimos inventore et neque? Iusto autem accusantium quidem voluptas quis adipisci nihil ut.
    </p>
    <div className='flex between gap-5 pl-96'>
    <img
    className='w-16 rounded-3xl' 
    src="https://p7.hiclipart.com/preview/402/235/698/businessperson-computer-icons-avatar-passport.jpg" alt="" />
    <div>
      <p className='text-lg text-amber-400 font-bold'>John doe</p>
      <p>principal</p>
    </div>
    </div>
  </div>

  <div className='w-1/2 mt-5 ml-96 pt-10 pl-5 bg-slate-100 rounded-md shadow-md'>
    <p className='text-xl font-sans'>
      Lorem ipsum dolor, sit amet consectetur adipisicing elit. Soluta cum, voluptatibus tempora illo consequatur quam nostrum molestiae. Dignissimos inventore et neque? Iusto autem accusantium quidem voluptas quis adipisci nihil ut.
    </p>
    <div className='flex between gap-5 pl-96'>
    <img
    className='w-16 rounded-3xl' 
    src="https://p7.hiclipart.com/preview/402/235/698/businessperson-computer-icons-avatar-passport.jpg" alt="" />
    <div>
      <p className='text-lg text-amber-400 font-bold'>Jane doe</p>
      <p>Founder & CEO</p>
    </div>
    </div>
  </div>
    </>
  )
}
