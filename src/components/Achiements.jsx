
export default function Achiements() {
  return (
    <>
    <div className="bg-white p-6 rounded-lg shadow-md">
    <div className="flex items-center">
      <div className="flex-grow border-t border-gray-400"></div>
      <span className="mx-4 text-gray-600">
        <h1 className="text-2xl font-bold mb-4">Our Achievements</h1>
      </span>
      <div className="flex-grow border-t border-gray-400"></div>
    </div>
  </div>
  <div className='p-6 grid grid-cols-2 gap-2 pt-5 pl-40 mx-10 '>
    <img src="src/img/result.jpg" alt="" className='w-80 rounded-lg shadow-md' />
    <img src="src/img/resultPluseone.jpg" alt="" className='w-72 rounded-lg shadow-md' />
    <img src="src/img/ussLss.jpg" alt="" className='w-80 rounded-lg shadow-md' />
    <img src="src/img/ussLss2.jpg" alt="" className='w-80 rounded-lg shadow-md' />
  </div>
    </>
  )
}
