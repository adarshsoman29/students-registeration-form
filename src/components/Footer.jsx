import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLocationDot, faPhone, faEnvelope, } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faInstagram, faTwitter } from '@fortawesome/free-brands-svg-icons';

export default function Footer() {
  return (
    <>
    <div className="bg-white p-6 rounded-lg shadow-md">
    <div className="flex items-center">
      <div className="flex-grow border-t border-gray-400"></div>
      <span className="mx-4 text-gray-600">
        <h1 className="text-2xl font-bold mb-4"></h1>
      </span>
      <div className="flex-grow border-t border-gray-400"></div>
    </div>
  </div>

    <footer className="bg-gray-800 text-white p-8">
    <div className="flex flex-wrap justify-between">
      
      <div className="flex-1">
        <h2 className="text-lg font-semibold mb-4">Contact Us</h2>
        <p>
        <FontAwesomeIcon icon={faLocationDot} /> Kallelibhagom, Karungappally, kollam
        </p>
        <p>
        <FontAwesomeIcon icon={faPhone} />+91 944611XXXX 
        </p>
        <p>
        <FontAwesomeIcon icon={faEnvelope} /> gurukulam@gmail.com
        </p>
        <div className='flex gap-5 pl-5 mt-5'>
        <FontAwesomeIcon icon={faInstagram} />
        <FontAwesomeIcon icon={faFacebook}/>
        <FontAwesomeIcon icon={faTwitter}/> 
        </div>
        
        <div className="mt-4">
          <a href="#" className="text-white mr-2"><i className="fab fa-facebook"></i></a>
          <a href="#" className="text-white mr-2"><i className="fab fa-twitter"></i></a>
          <a href="#" className="text-white"><i className="fab fa-instagram"></i></a>
        </div>
      </div>

   
      <div className="flex-1 mt-4 lg:mt-0">
        <h2 className="text-lg font-semibold mb-4">Quick Links</h2>
        <ul>
          <li><a href="#" className="text-gray-300 hover:text-white">Home</a></li>
          <li><a href="#" className="text-gray-300 hover:text-white">About Us</a></li>
          <li><a href="#" className="text-gray-300 hover:text-white">Services</a></li>
          <li><a href="#" className="text-gray-300 hover:text-white">Contact</a></li>
        </ul>
      </div>

    
      <div className="flex-1 mt-4 lg:mt-0">
        
        <h2 className="text-lg font-semibold mb-4">Additional Content</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vitae commodo libero.</p>
      </div>
    </div>
  </footer>
    </>
  )
}
