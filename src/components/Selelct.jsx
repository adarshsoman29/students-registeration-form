/* eslint-disable react/prop-types */
import { useFormContext } from 'react-hook-form';
import { AnimatePresence, motion } from 'framer-motion';
import { MdError } from 'react-icons/md';
import { findInputError, isFormInvalid } from '../utils';

// eslint-disable-next-line react/prop-types
export default function Select({ label, id, placeholder, name, validation, options }) {
  const {
    register,
    formState: { errors },
  } = useFormContext();

  const inputError = findInputError(errors, name);
  const isInvalid = isFormInvalid(inputError);

  return (
    <>
      <div className="mt-5">
        <label htmlFor={id} className="block text-gray-700 font-semibold">
          {label}
        </label>
        <AnimatePresence mode="wait" initial={false}>
          {isInvalid && (
            <InputError message={inputError.error.message} key={inputError.error.message} />
          )}
        </AnimatePresence>
      </div>
      <select
        id={id}
        {...register(name, validation)}
        className="border rounded-md py-1 px-2 w-full font-medium"
      >
        <option value="" disabled>
          {placeholder}
        </option>
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
    </>
  );
}

const InputError = ({ message }) => {
  return (
    <motion.p
      className="flex items-center gap-1 px-2 font-semibold text-red-500 bg-red-100 rounded-md"
      {...framer_error}
    >
      <MdError />
      {message}
    </motion.p>
  );
};

const framer_error = {
  initial: { opacity: 0, y: 10 },
  animate: { opacity: 1, y: 0 },
  exit: { opacity: 0, y: 10 },
  transition: { duration: 0.2 },
};
