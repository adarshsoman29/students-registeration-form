import { Outlet } from "react-router-dom";
import { Link } from "react-router-dom";
import Footer from "./Footer";


export default function Header() {
  return (
    <>
     <nav className="flex flex-col lg:flex-row  lg:justify-between  lg:items-center px-10 py-5 bg-rose-950">
        <div>
        <Link className="text-white font-PT+Sans">GURUKULAM</Link>
        </div>
        <ul className="flex flex-col lg:flex-row  lg:justify-between  lg:items-center">
                <li className="mr-5 text-white text text-lg font-semibold"><Link to="/">Home</Link></li>
                <li className="mr-5 text-white text-lg font-semibold"><Link to="/registeration">Registration</Link></li>
                <li className="mr-5 text-white text-lg font-semibold"><Link to="/login">Login</Link></li>
                <li><Link to="/contact" className="text-white text-lg font-semibold mr-5">Contact</Link></li>
                <li><Link to="/profile" className="text-white text-lg font-semibold">Profile</Link></li>
        </ul>
    </nav>
    <div id="detail">
        <Outlet />
      </div>
      <Footer></Footer>
      
    </>
  )
}
