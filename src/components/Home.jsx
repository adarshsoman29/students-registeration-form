import Achiements from "./Achiements";
import Disciplines from "./Disciplines";
import LearnAbout from "./LearnAbout";
import MainPhoto from "./MainPhoto";
import WhyGurukulam from "./WhyGurukulam";


export default function Home() {
  return (
    <>
    <MainPhoto></MainPhoto>
    <Disciplines></Disciplines>
    <Achiements></Achiements>
    <LearnAbout></LearnAbout>
    <WhyGurukulam></WhyGurukulam>
    </>
  )
}
