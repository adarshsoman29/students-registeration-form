export const name_of_student_validation = {
  name : "name_of_student",
  label: 'Name',
  type: 'text',
  id: 'name',
  placeholder: 'write your name ...',
}

export const name_Of_school_validation = {
  name: 'name_Of_School',
  label: 'Name of school',
  type: 'text',
  id: 'name',
  placeholder: 'write your school name ...',
}

export const class_validation = {
  label :"Class",
  id :"selectOption",
  name :"class",
  options:[
    { value: '', label: 'Select your class' },
    { value: 'V', label: 'V' },
    { value: 'VI', label: 'VI' },
    { value: 'VII', label: 'VII' },
    { value: 'VIII', label: 'VIII' },
    { value: 'IX', label: 'IX' },
    { value: 'X', label: 'X' },
    { value: 'XI', label: 'XI' },
    { value: 'XII', label: 'XII' },
  ]
}

export const Medium_Validation = {
  label :"Select medium",
  id : "medium",
  name :"medium",
  options :[
    { label: " English", value: "English" },
    { label: " Malayalam", value: "Malayalam" },
  ],   
};

export const Syllabus_Validation = {
  label :"Select syllabus",
  id : "syllabus",
  name :"syllabus",
  options :[
    { label: " State", value: "state" },
    { label: " CBSE", value: "cbse" },
    { label: " ICSE", value: "icse" },
  ],   
};

export const date_of_birth_validation = {
  name: 'dob',
  label: "Date of birth",
  type: 'date',
  id: 'dob',
}


export const Name_Of_mother_validation = {
  name: 'name_Of_mother',
  label: 'Name of mother',
  type: 'text',
  id: 'name',
  placeholder: "write your mother's name ...",
}

export const Name_Of_father_validation = {
  name: 'name_Of_father',
  label: 'Name of father',
  type: 'text',
  id: 'name',
  placeholder: "write your father's name ...",
}

export const address_Validation = {
  name: 'address',
  label: 'Address',
  type: "textarea",
  multiline: true,
  id: 'address',
  placeholder: 'write address ...',
}

export const PhoneNumber_Validation = {
  name: 'phoneNumber',
  label: 'Phone Number',
  type: 'tel', // Use type 'tel' for phone numbers
  id: 'phoneNumber',
  placeholder: 'Enter your phone number...',
  
};

export const whatsappNumber_Validation = {
  name: 'WhatsappNumber',
  label: 'Whatsapp number',
  type: 'tel', // Use type 'tel' for phone numbers
  id: 'phoneNumber',
  placeholder: 'Enter your whatsapp number...',
 
};

export const email_Validation = {
  name: 'email',
  label: 'email address (optional)',
  type: 'email',
  id: 'email',
  placeholder: 'write an email address',
}

//ask question form validation

// export const name_validation = {
//   name : "yourName",
//   label: 'Your full name',
//   type: 'text',
//   id: 'yourName',
//   placeholder : 'full name'
// }

// export const subject_validation = {
//   name : "subject",
//   label: 'Your subject',
//   type: 'text',
//   id: 'subject',
//   placeholder : 'Your subject'
// }

// export const message_Validation = {
//   name: 'message',
//   label: 'message',
//   type: "textarea",
//   multiline: true,
//   id: 'message',
//   placeholder: 'write your message here',
// }