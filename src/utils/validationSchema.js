import * as yup from 'yup';

export const validationSchema = yup.object().shape({
   name_of_student : yup.string().trim().required('required').max(30, '30 charachter max'),
   class : yup.string().required('Please select an option'),
   medium : yup.string().required('select a medium'),
   name_Of_mother : yup.string().trim().required('required').max(30, '30 charachter max'),
   name_Of_father : yup.string().trim().required('required').max(30, '30 charachter max'),
   name_Of_School : yup.string().trim().required('required').max(30, '30 charachter max'),
   dob : yup.date().max(new Date()) .required('Date of birth is required'),
   address : yup.string().trim().required('required').max(120, '120 characters max'),
   phoneNumber : yup.string().trim().required('Phone number is required').matches(/^[0-9]{10}$/, 'Invalid phone number format'),
   WhatsappNumber : yup.string().trim().required('whatsapp number is required').matches(/^[0-9]{10}$/, 'Invalid phone number format'),
   email : yup.string().trim().matches( /^$|^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,'not valid' ).optional(),
   // yourName: yup.string().trim().required('required').max(30, '30 charachter max'),
   // subject : yup.string().trim().required('required').max(60, '60 charachter max'),
   // message : yup.string().trim().required('required').max(120, '120 characters max'),
  });