import React from 'react'
import ReactDOM from 'react-dom/client'
//import App from './App.jsx'
import './index.css'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import Header from './components/Header';
import ErrorPage from './components/ErrorPage';
import Registeration from './components/Registeration';
import Home from './components/Home';
//import ContactUs from './components/ContactUs';
import StudentProfile from './components/StudentProfile';

const router = createBrowserRouter([
  {
    path :'/',
    element : <Header></Header>,
    errorElement : <ErrorPage></ErrorPage>,
    children : [
      {
        path : "/registeration",
        element : <Registeration></Registeration>
      },
      {
        path : "/",
        element : <Home></Home>
      },
      {
        // path :"/contact",
        //  element : <ContactUs></ContactUs>
      },{
        path : "/profile",
        element : <StudentProfile></StudentProfile>
      }
    ]
  }
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router}></RouterProvider>
  </React.StrictMode>,
)
